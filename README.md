## gem2xbps

Tool to generate XBPS source template files from Ruby gems

Requires the specified gem and all its runtime dependencies to be localy
installed before. It does not matter, if they are installed
* systemwide with `gem install <gem name>` or by xbps-install
* in an user profile with `gem install --user <gem name>`
* in a bundler environment
  put `gem <gem name>, <version>` into *Gemfile*,
  do an install with `bundle install --path .bundle`
  run generator inside environment with `bundle exec gem2xbps <gem name>`

Don't forget add distfiles checksum with *xgensum* utility.

Gem's specification may miss some information like C -devel library
dependency package or license name, so you need add them manually.
